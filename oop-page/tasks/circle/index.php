<?php
include 'Circle.php';

// 1. Variantas: 
// + → Iškarto paruošti duomenys, nekviečiami metodai, todėl nėra rizikos dėl klaidų
// - → Duomenys atskiruose kintamuosiuose, bereikalingai naudojama atmintis
$radiuses = [3, 5, 7];
$areas = [];
$perimeters = [];
foreach ($radiuses as $r) {
  $circle = new Circle($r);
  $areas[] = $circle->getArea();
  $perimeters[] = $circle->getPerimeter();
}

// 2. Variantas: 
// + → Duomenys grupuojami pagal prasmę
// - → Klaidų atsakomybė perkeliama į atvaizdavimo logiką
$circles = [];
foreach ($radiuses as $r) {
  $circles[] = new Circle($r);
}

// 3. Variantas
// + → Duomenys grupuojami pagal prasmę
// + → Nekeviečiami metodai atvaizdavimo metu
$circlesData = array_map(function($r){
  $circle = new Circle($r);
  return [
    'r' => $r,
    'perimeter' => $circle->getPerimeter(),
    'area' => $circle->getArea()
  ];
}, $radiuses);
?>

<p>
  <a href="https://edabit.com/challenge/CNubhddXNa4j5X49H" target="blank">
    Make a Circle with OOP
  </a>
</p>
<div class="row">
  <div class="col">
    <h4>1. Atskiruose masyvuose</h4>
    <?php for ($i = 0; $i < count($radiuses); $i++) : ?>
      <ul>
        <li>Radius: <?= $radiuses[$i] ?></li>
        <li>Perimeter: <?= $perimeters[$i] ?></li>
        <li>Area: <?= $areas[$i] ?></li>
      </ul>
    <?php endfor; ?>
  </div>

  <div class="col">
    <h4>2. Su klasės objektais</h4>
    <?php foreach ($circles as $circle) : ?>
      <ul>
        <li>Radius: <?= $circle->r ?></li>
        <li>Perimeter: <?= $circle->getPerimeter() ?></li>
        <li>Area: <?= $circle->getArea() ?></li>
      </ul>
    <?php endforeach ?>
  </div>

  <div class="col">
    <h4>3. Su suformuotais duomenimis</h4>
    <?php foreach ($circlesData as $circle) : ?>
      <ul>
        <li>Radius: <?= $circle['r'] ?></li>
        <li>Perimeter: <?= $circle['perimeter'] ?></li>
        <li>Area: <?= $circle['area'] ?></li>
      </ul>
    <?php endforeach ?>
  </div>
</div>