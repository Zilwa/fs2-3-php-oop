<?php
class Circle
{
  public $r;

  public function __construct($r)
  {
    $this->r = $r;
  }

  public function getArea()
  {
    return pi() * $this->r ** 2;
  }
  
  public function getPerimeter()
  {
    return 2 * pi() * $this->r;
  }
}
