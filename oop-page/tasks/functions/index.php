<h4>
  18. Sukurti funkciją, kuri ima masyvą ir grąžina visų
  jo elementų sumą
</h4>
<?php
function arr_sum(array $arr): float
{
  return array_reduce($arr, function ($carry, $el) {
    return $carry + $el;
  });
}

var_dump(arr_sum([7, 8, 9]));
var_dump(arr_sum([7, 8, -8, -7]));
?>


<h4>
  22. Sukurti funkciją, kuri ima masyvą ir išrikiuja jo ele-
  mentus nuo mažiausio iki didžiausio ir grąžina tą masyvą.
</h4>
<?php

$numbers = [7, 8, 1, 7, 8, 4, 5, 12, 4, 5, 7, 8, 8, 6, 3, 4, 99];

function arr_sort(array $arr): array
{
  // 1. YZY
  // sort($arr);
  // return $arr;
  // 2. 
  // usort($arr, function ($elementOnTheLeft, $elementOnTheRight) {
  //   /* Palygini funkcija turi grąžinti vieną iš 3 skaitinių reikšmių
  //     0< - pirmuoju argumentu paduotas elementas yra aukštesnio prioriteto | Tokiu atveju Elementai, apkeičiami vietomis
  //     0  - lyginami masyvo elementai yra lygaus prioriteto
  //     0> - pirmuoju argumentu paduotas elementas yra žemesnio prioriteto
  //   */
  //   // Dėkinga situacija lyginant skaičius
  //   return $elementOnTheLeft - $elementOnTheRight;
  //   // Dažniausiai naudojama logika
  //   // if ($elementOnTheLeft > $elementOnTheRight) return 1;
  //   // return $elementOnTheLeft === $elementOnTheRight ? 0 : -1;
  // });

  // 3. Bubble sort
  for ($i = 0; $i < count($arr) - 1; $i++) {
    for ($j = 0; $j < count($arr) - 1 - $i; $j++) {
      if ($arr[$j] > $arr[$j + 1]) {
        $temp = $arr[$j];
        $arr[$j] = $arr[$j + 1];
        $arr[$j + 1] = $temp;
      }
    }
  }
  return $arr;
}

$numbersSorted = arr_sort($numbers);
var_dump($numbersSorted);
?>