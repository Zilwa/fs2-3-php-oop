<?php
function printArray(array $arr, string $title): void
{
  $indexColumns = '';
  $valueColumns = '';
  foreach ($arr as $index => $value) {
    $indexColumns .= '<div class="col">' . $index . '</div>';
    $valueColumns .= '<div class="col">' . $value . '</div>';
  }
?>
  <h4><?= $title ?></h4>
  <div class="array">
    <div class="row gx-0 fw-bold">
      <div class="col">Values:</div>
      <?= $valueColumns ?>
    </div>
    <div class="row gx-0">
      <div class="col">Indexes:</div>
      <?= $indexColumns ?>
    </div>
  </div>
<?php
}

// 1. Persirašykite masyvą sudarytą iš skaičių
$numbers = [1, 2, 3, 4, -8, 5, 6, 9, 7, 8];
printArray($numbers, '1. Persirašykite masyvą sudarytą iš skaičių');

// 2. Padauginti esamo masyvo narius iš 2
function arrayValueDouble(array $arr): array
{
  foreach ($arr as $value)
    $res[] = $value * 2;
  return $res;
}
$arrayValuesDoubled = arrayValueDouble($numbers);
printArray($arrayValuesDoubled, '2. Padauginti esamo masyvo narius iš 2');




?>