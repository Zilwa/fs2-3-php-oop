<?php
class Allergies
{
  // do not alter this enum
  public static $allergens = [
    'Eggs' =>  1,
    'Peanuts' =>  2,
    'Shellfish' =>  4,
    'Strawberries' =>  8,
    'Tomatoes' =>  16,
    'Chocolate' =>  32,
    'Pollen' =>  64,
    'Cats' =>  128
  ];
  // write your code below this line
  private $__name;
  private $__score;

  // constructors 
  public function __construct($name, $allergies = null)
  {
    $this->__name = $name;
    if (!isset($allergies)) {
      $this->__score = 0;
    } else if (is_string($allergies)) {
      // paversti $allergies stringų masyvu naudojant funkciją explode
      // tais gautais stringai reikia iš traukti attinkamas reikšmes iš 
      // <self::$allergen> masyvo ir sudėti.
      // Gautą rezultatą reikia priskirti į savybę score
      $allergens = explode(' ', $allergies);
      $score = 0;
      foreach ($allergens as $allergen) {
        $score += self::$allergens[$allergen];
      }
      $this->__score = $score;
    } else if (is_int($allergies)) {
      $this->__score = $allergies;
    }
  }

  // properties

  // __set metodas, vykdomas tuomet kai klasės išorėje bandoma nustatyti savybės reikšmę,
  // kuri neegzistuoja, yra private arba protected
  public function __set($name, $value)
  {
    switch ($name) {
      case 'score':
        throw new Exception('score property is readonly');
      case 'name':
        throw new Exception('name property is readonly');
    }
    // $this->$name = $value;
  }

  // __get metodas, vykdomas tuomet kai klasės išorėje bandoma gauti savybės reikšmę,
  // kuri neegzistuoja, yra private arba protected
  public function __get($name)
  {
    switch ($name) {
      case 'score':
        return $this->getScore();
      case 'name':
        return $this->getName();
    }
  }

  // Sutartinė praktika gauti private arba protected savybę
  // Jo pasiekiamumas yra private, todėl nes norime supaprastinti sintaksę, 
  // naudodami __get
  private function getName()
  {
    // Patikrinimai
    return $this->__name;
  }

  // Sutartinė praktika gauti private arba protected savybę
  // Jo pasiekiamumas yra private, todėl nes norime supaprastinti sintaksę, 
  // naudodami __get
  private function getScore()
  {
    // Patikrinimai
    return $this->__score;
  }
  // methods

  private function formAllergenArray()
  {
    // Logiką, kuri naudodami savybę <$this->__score> sudaro atitinkamų alergenų <string> masyvą
    $binary = decbin($this->__score);
    $allergensBinArr = array_reverse(str_split($binary));
    $allergenStringArray = [];
    foreach ($allergensBinArr as $powerOf2 => $binDigit) {
      if ($binDigit === '1') {
        // ------------------------------google: php array_search ------------------------------
        // 1. Suskaičiuoti reikšmę, kurią atstovauja bitas: 2 ** $i * $binDigit
        $alergenCode =  2 ** $powerOf2 * $binDigit;
        // 2. Naudojant array_search metodą, surasti atitinkamą raktą pagal reikšmę <self::allergies>, eilutės[5:14]
        $alergenName = array_search($alergenCode, self::$allergens);
        // 3. Surasta raktą <self::allergies> masyve reikia įdėti į formuojama masyvą $allergens
        $allergenStringArray[] = $alergenName;
      }
    }
    return $allergenStringArray;
  }

  public function __toString()
  {
    $allergenStrArr = $this->formAllergenArray();
    // Grąžinite suformuotą stringą pagal šį pvz.
    "Fred is allergic to Peanuts."; // Jeigu alergiškas vienam alergenui
    "Joe is allergic to Eggs and Pollen."; // Jeigu alergiškas dviem
    "Rob is allergic to Peanuts, Strawberries, Chocolate and Cats."; // Jeigu alergiškas N alergenų
    if (count($allergenStrArr) === 0)
      return "$this->__name has no allergies!";
    // 1 --------------
    $result = "$this->__name is allergic to ";
    $allergensCount = count($allergenStrArr);
    for ($i = 0; $i < $allergensCount; $i++) {
      $result .= $allergenStrArr[$i];
      $nthFromEnd = $allergensCount  - $i;
      switch ($nthFromEnd) {
        case 1:
          $result .= '.';
          break;
        case 2:
          $result .= ' and ';
          break;
        default:
          $result .= ', ';
      }
    }
    return $result;
    // 2 -------------------
    if (count($allergenStrArr) === 1)
      return "$this->__name is allergic to $allergenStrArr[0].";
    if (count($allergenStrArr) === 2)
      return "$this->__name is allergic to $allergenStrArr[0] and $allergenStrArr[1].";
    return implode([
      "$this->__name is allergic to ",
      implode(', ', array_slice($allergenStrArr, 0, -2)),
      ', ',
      implode(' and ', array_slice($allergenStrArr, -2)),
      '.'
    ]);
    // 3 -----------------------
    $lastAlergen = array_pop($allergenStrArr);
    // Reikia padarytio atskira spausdinima vieno alergeno atveju
    return "$this->__name is allergic to " . implode(', ', $allergenStrArr) . " and $lastAlergen.";
  }

  public function isAllergicTo($allergen)
  {
    return (self::$allergens[$allergen] & $this->__score) !== 0;
  }

  public function addAllergy($allergen)
  {
    $this->__score |= self::$allergens[$allergen];
  }

  public function deleteAllergy($allergen)
  {
    $potencial = $this->__score ^ self::$allergens[$allergen];
    if ($potencial < $this->__score) $this->__score = $potencial;
  }
}
