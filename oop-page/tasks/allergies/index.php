<?php
// https://edabit.com/challenge/AiANZNMxLdvMSgBZ4
include 'Allergies.php';
$mary = new Allergies("Mary");
$suzy = new Allergies("Suzy", 8);
$joe = new Allergies("Joe", 65);
$rob = new Allergies("Rob", "Peanuts Chocolate Cats Strawberries");

var_dump([$mary, $suzy, $joe, $rob]);
?>