<?php
$books = [
  [
    'title' => 'Jane Eyre',
    'author' => 'Charlotte Bronte',
    'year' => 1847,
    'genre' => 'Novel'
  ],
  [
    'title' => 'King\'s case',
    'author' => 'Viktoria Aveyard',
    'year' => 2017,
    'genre' => 'Romans'
  ],
  [
    'title' => 'Mein Kampf',
    'author' => 'Adolf Hitler',
    'year' => 1925,
    'genre' => 'Autobiography'
  ],
  [
    'title' => 'Use your memory',
    'author' => 'Tony Buzan',
    'year' => 2003,
    'genre' => 'Textbooks'
  ]
];
?>
<h4>27. Išvesti visus knygų masyvo elementus lentele;</h4>

<table class="table table-striped">
  <thead class="thead bg-dark text-light">
    <tr>
      <th>Title</th>
      <th>Author</th>
      <th>Year</th>
      <th>Genre</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($books as $book) : ?>
      <tr>
        <td><?= $book['title'] ?></td>
        <td><?= $book['author'] ?></td>
        <td><?= $book['year'] ?></td>
        <td><?= $book['genre'] ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<table class="table table-striped">
  <thead class="thead bg-dark text-light">
    <tr>
      <?php foreach (array_keys($books[0]) as $header) : ?>
        <th><?= ucfirst($header) ?></th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($books as $book) : ?>
      <tr>
        <?php foreach ($book as $val) : ?>
          <td><?= $val ?></td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>