<?php
include_once './libraries/TextTableRenderer.php';
include_once './libraries/HtmlTableRenderer.php';
include_once './data/tableRendererData.php';
/**
 * creates array of strings which consists of christmas tree lines
 *
 * @param integer $height
 * @return array
 */
function tree(int $height): array
{
  $lines = [];
  for ($floor = 1; $floor <= $height; $floor++) {
    $spacesCount = $height - $floor;
    $symbolCount = $floor * 2 - 1;
    $lines[] = str_repeat(' ', $spacesCount) . str_repeat('#', $symbolCount) . str_repeat(' ', $spacesCount);
  }
  return $lines;
};

/**
 * printTree - function to print string representation of christmas tree from array
 *
 * @param array $tree
 * @return void
 */
function printTree(array $tree): void
{
  echo '<pre>';
  foreach ($tree as $level) echo $level . "<br>";
  echo '</pre>';
};

$resultData = array_map(function ($height) {
  return [
    'height' => $height,
    'tree' => tree($height)
  ];
}, [5, 12, 8]);
// Lentelės braižymas
$treeTableRenderer = new TextTableRenderer($tableFormats['christmasTreeTableFormat']);
$treeHtmlTR = new HtmlTableRenderer($tableFormats['christmasTreeTableFormat']);

// echo 'echo $treeTableRenderer->formatTable();<br>' . $treeTableRenderer->formatTable();
// echo 'echo $treeTableRenderer->formatTable(1);<br>' . $treeTableRenderer->formatTable(1);
// echo 'echo $treeTableRenderer->formatTable(2);<br>' . $treeTableRenderer->formatTable(2);
echo 'echo $treeTableRenderer->formatTable(3);<br>' . $treeTableRenderer->formatTable(3);
echo 'echo $treeHtmlTR->formatTable(3);<br>' . $treeHtmlTR->formatTable(3);
// echo 'echo $treeTableRenderer->formatTable(4);<br>' . $treeTableRenderer->formatTable(4);
// echo 'echo $treeTableRenderer->formatTable(5);<br>' . $treeTableRenderer->formatTable(5);
?>

<div class="row">
  <?php foreach ($resultData as $treeData) : ?>
    <div class="col">
      <h4>Elgutės aukštis: <?= $treeData['height'] ?></h4>
      <div><?= printTree($treeData['tree']) ?></div>
    </div>
  <?php endforeach; ?>
</div>