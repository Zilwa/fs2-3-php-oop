<?php
include_once './libraries/TextTableRenderer.php';
include_once './libraries/HtmlTableRenderer.php';
include_once './data/tableRendererData.php';
$people1 = [
  ["name" => "Kests", "surname" => "Gersssssss", "age" => 17555555, "sex" => "vyrs"],
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
  ["name" => "Kestssssssssss", "surname" => "Gers", "age" => 17, "sex" => "vyrsssssss"],
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
];
$people2 = [
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
];
$people3 = [
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
  ["name" => "Kests", "surname" => "Gers", "age" => 17, "sex" => "vyrs"],
];
$peopleTableRender = new TextTableRenderer($tableFormats['peopleTableFormat']);
$peopleHtmlTR = new HtmlTableRenderer($tableFormats['peopleTableFormat']);
?>
<h4>Žmonių lentelės atvaizdavimas</h4>
<p>
  Atvaizduoti žmonių lentelę:
<h5>Stulpeliai</h5>
<ul>
  <li>Vardas</li>
  <li>Pavardė</li>
  <li>Amžius</li>
  <li>Lytis</li>
</ul>
Tai padaryti naudojant klasę <span class="fw-bold">TableRenderer</span>
<ol>
  <li>Susikontruoti objektą <strong>$peopleTableRenderer</strong></li>
  <li>Pirmu parametru perduoti lentelės antraštes</li>
  <li>Antru parametru perduoti masyvą su aprašytomis funkcijos, kurios atspausdina letelės duomenis</li>
  <li>Trečiu parametru perduoti, funkciją, kuri paruoš duomenis, kurie bus perduodamei
    <strong>formatTable</strong> metodo metu
  </li>
  <li>Naudojant objektą <strong>$peopleTableRenderer</strong> kviesti metodą <strong>formatTable</strong>
    3 kartus perduodant 3 skirtingus žmonių masyvus.
  </li>
</ol>
</p>
<h4>Rezultatai</h4>
<p><?= $peopleTableRender->formatTable($people1) ?></p>
<p><?= $peopleTableRender->formatTable($people2) ?></p>
<p><?= $peopleTableRender->formatTable($people3) ?></p>

<p><?= $peopleHtmlTR->formatTable($people1) ?></p>
<p><?= $peopleHtmlTR->formatTable($people2) ?></p>
<p><?= $peopleHtmlTR->formatTable($people3) ?></p>

