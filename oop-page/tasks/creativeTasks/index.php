<h4>Dices</h4>
<?php
function diceRoll(): void
{
  $gameLog = "";
  do {
    $diceOne = random_int(1, 6);
    $diceTwo = random_int(1, 6);
    $suma = $diceOne + $diceTwo;

    $gameLog .= "Pirmas kauliukas: " . $diceOne . " Antras kauliukas: " . $diceTwo . "<br>";
    $gameLog .= "Suma: " . $suma . ".<br>";

    if ($suma < 5) {
      $gameLog .= "Tu pralaimejai ://///////// <br>"; break;
    } else if ($diceOne === 6 && $diceTwo === 6) {
      $gameLog .= "Tu laimejai !!!!!!!!!! <br>"; break;
    } else if ($suma > 5 && $diceOne === $diceTwo) {
      $gameLog .= "Jus laimejote bilieta i kina :)))))))))) <br>"; break;
    }
  } while (true);
  echo $gameLog . "<br>";
}
diceRoll();
?>
<h4>Boxes</h4>
<?php
$items = [1, 7, 8, 1, 2, 8, 7, 4, 2, 3, 2, 4, 1, 6, 3, 7, 4, 1, 5, 6, 5, 2, 1, 12, 4];
define('MAX_BOX_WEIGHT', 8);
// const MAX_BOX_WEIGHT = 8;
$boxes = [];
$availableBoxIndex = 0;

foreach ($items as $itemWeight) {
  if ($itemWeight > MAX_BOX_WEIGHT) continue;
  if ($itemWeight === MAX_BOX_WEIGHT) {
    array_unshift($boxes, [$itemWeight]);
    $availableBoxIndex++;
    continue;
  };
  if (count($boxes) - 1 < $availableBoxIndex) $boxes[] = [];

  for ($j = $availableBoxIndex; $j < count($boxes); $j++) {
    $potencialWeight = $itemWeight + array_sum($boxes[$j]);
    if ($potencialWeight < MAX_BOX_WEIGHT) {
      $boxes[$j][] = $itemWeight;
      break;
    } else if ($potencialWeight === MAX_BOX_WEIGHT) {
      $boxes[$j][] = $itemWeight;
      array_unshift($boxes, ...array_splice($boxes, $j, 1));
      $availableBoxIndex++;
      break;
    } else if ($j + 1 === count($boxes)) {
      $boxes[] = [$itemWeight];
      break;
    }
  }
}

var_dump($boxes);
