<?php
$folders = glob('tasks/*', GLOB_ONLYDIR);
foreach ($folders as $key => $value) {
  $parts = explode('/', $value);
  $folderName = end($parts);
  $folders[$key] = $folderName;
}
?>

<?php foreach ($folders as $folder) : ?>
  <article class="p-3 shadow mb-5">
    <h3><?= ucfirst($folder) ?></h3>
    <?php include $folder . '/index.php' ?>
  </article>
<?php endforeach; ?>

