<?php
require_once 'TableRenderer.php';
class TextTableRenderer extends TableRenderer
{
  private array $colWidths;
  private int $tableWidth;
  private string $tableLine;

  public function __construct($parentProps)
  {
    parent::__construct($parentProps);
  }

  private function calcTableSizings($headersData, $rowsData): void
  {
    $colCount = 0;
    $headerCount = count($this->headerRowsFormat);
    $rowCount = count($rowsData);
    $this->colWidths = [];

    foreach ($this->headerRowsFormat as $header) {
      $headerColCount = count($header);
      if ($headerColCount > $colCount)
        $colCount = $headerColCount;
    }

    for ($colIndex = 0; $colIndex < $colCount; $colIndex++) {
      $colWidth = 0;
      for ($headerIndex = 0; $headerIndex < $headerCount; $headerIndex++) {
        if (!isset($this->headerRowsFormat[$headerIndex][$colIndex]))
          continue;
        $headerColValue = $this->headerRowsFormat[$headerIndex][$colIndex];
        if (is_callable($headerColValue))
          $headerColValue = $headerColValue($headersData);

        $colStrCount = mb_strlen($headerColValue);
        if ($colStrCount > $colWidth)
          $colWidth = $colStrCount;
      }

      for ($rowIndex = 0; $rowIndex < $rowCount; $rowIndex++) {
        if (!isset($this->dataRowFormat[$colIndex]))
          continue;
        $rowColValue = $this->dataRowFormat[$colIndex];
        if (is_callable($rowColValue))
          $rowColValue = $rowColValue($rowsData[$rowIndex]);

        $colStrCount = mb_strlen($rowColValue);
        if ($colStrCount > $colWidth)
          $colWidth = $colStrCount;
      }
      $this->colWidths[] = $colWidth;
    }
    $this->tableWidth = array_sum($this->colWidths) + $colCount * 2 + $colCount + 1;
    $this->tableLine = '+' . str_repeat('-', $this->tableWidth - 2) . '+<br>';
  }

  protected function formatHeaders($data = []): string
  {
    $headersView = $this->tableLine;
    foreach ($this->headerRowsFormat as $header) {
      $headersView .= '|';
      foreach ($header as $colIndex => $col) {
        if (is_callable($col))
          $col = $col($data);
        $valLength = mb_strlen($col);
        $maxColumnLength = $this->colWidths[$colIndex];
        if ($valLength < $maxColumnLength) {
          $spacesCount = $maxColumnLength - $valLength;
          $col = str_repeat(' ', $spacesCount) . $col;
        }
        $headersView .= ' ' . $col . ' |';
      }
      $colCount = count($this->colWidths);
      if (!isset($colIndex)) {
        $headersView .= str_repeat(' ', $this->tableWidth - 2) . '|';
      } else if ($colIndex < $colCount - 1) {
        $spacesCount = 0;
        for ($i = $colIndex + 1; $i < $colCount; $i++) {
          $spacesCount += $this->colWidths[$i];
        }
        $emptyCols = $colCount - $colIndex - 1;
        $spacesCount +=  2 * $emptyCols + $emptyCols - 1;
        $headersView .= str_repeat(' ', $spacesCount) . '|';
      }
      $headersView .= '<br>' . $this->tableLine;
    }
    return $headersView;
  }

  protected function formatDataRows($data = []): string
  {
    $dataView = '';
    $i = 0;
    do {
      $dataView .= '|';
      $rowData = $data[$i++] ?? [];
      // Vienos eilutės atvaizdavimas - START
      foreach ($this->dataRowFormat as $colIndex => $dataColumnFunction) {
        if (!isset($dataColumnFunction)) {
          $spaceCount = $this->colWidths[$colIndex] + 2;
          $dataView .= str_repeat(' ', $spaceCount);
          $dataView .= isset($this->dataRowFormat[$colIndex + 1]) ? '|' : ' ';
        } else {
          $res = $dataColumnFunction($rowData);
          $res = strval($res);
          $charCount = mb_strlen($res);
          $spaceCount = $this->colWidths[$colIndex] - $charCount;
          $res = str_repeat(' ', $spaceCount >= 0 ? $spaceCount : 0) . $res;
          $dataView .= ' ' . $res . ' |';
        }
      }
      $dataView .= '<br>';
      // Vienos Eilutės atvaizdavimas - END
    } while ($i < count($data));
    return $dataView . $this->tableLine;
  }

  public function formatTable($data = null): string
  {
    extract(($this->dataRenderingFunction)($data));
    $this->calcTableSizings($headersData ?? [], $rowsData ?? []);
    return
      '<pre>'
      . $this->formatHeaders($headersData ?? [])
      . $this->formatDataRows($rowsData ?? [])
      . '</pre>';
  }
}
