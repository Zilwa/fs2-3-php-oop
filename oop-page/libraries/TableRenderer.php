<?php
abstract class TableRenderer
{
  protected array $headerRowsFormat;
  protected array $dataRowFormat;
  protected $dataRenderingFunction;

  public function __construct($parentProps)
  {
    extract($parentProps);
    $this->headerRowsFormat = $headerRowsFormat ?? [];
    $this->dataRowFormat = $dataRowFormat ?? [];
    $this->dataRenderingFunction = $dataRenderingFunction ?? null;
  }

  abstract protected function formatHeaders($data = []): string;
  abstract protected function formatDataRows($data = []): string;
  abstract public function formatTable($data = null): string;
}
