<?php
require_once 'TableRenderer.php';
class HtmlTableRenderer extends TableRenderer
{
  private int $colCount;

  public function __construct($parentProps)
  {
    parent::__construct($parentProps);
    $this->colCount = $this->calcColCount();
  }

  private function calcColCount(): int
  {
    $maxColCount = 0;
    foreach ([...$this->headerRowsFormat, $this->dataRowFormat] as $row) {
      $colCount = count($row);
      if ($colCount > $maxColCount)
        $maxColCount = $colCount;
    }
    return $maxColCount;
  }

  protected function formatHeaders($data = []): string
  {
    $headers = '';
    foreach ($this->headerRowsFormat as $row) {
      $headers .= '<tr>';
      for ($i = 0; $i < $this->colCount; $i++) { 
        $col = $row[$i] ?? '';
        if (is_callable($col))
          $col = $col($data);
        $headers .= "<th>$col</th>";
      }
      $headers .= '</tr>';
    }
    return $headers;
  }
  protected function formatDataRows($data = []): string
  {
    /*
      <tr>
        <td></td>
          . . .
        <td></td>
        <td></td>
      </tr>
      . . .
      <tr>
        <td>
       . . .
      </tr>
    */
    return '';
  }
  public function formatTable($data = null): string
  {
    extract(($this->dataRenderingFunction)($data));
    return
      '<table class="table table-striped"><thead class="thead bg-dark text-light">'
      . $this->formatHeaders($headersData ?? [])
      . '</thead><tbody>'
      . $this->formatDataRows($rowsData ?? [])
      . '</tbody></table>';
  }
}
