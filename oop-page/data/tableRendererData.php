<?php
$tableFormats = [
  "peopleTableFormat" => [
    "headerRowsFormat" => [
      ['Vardas', 'Pavardė', 'Amžius', 'Lytis']
    ],
    "dataRowFormat" => [
      function (array $data = []): string {
        return $data['name'] ?? 'Not set';
      },
      function (array $data = []): string {
        return $data['surname'] ?? 'Not set';
      },
      function (array $data = []): string {
        return $data['age'] ?? 'Not set';
      },
      function (array $data = []): string {
        return $data['sex'] ?? 'Not set';
      }
    ],
    "dataRenderingFunction" => function (array $people): array {
      return [
        "headersData" => [],
        "rowsData" => $people
      ];
    }
  ],
  "christmasTreeTableFormat" =>  [
    "headerRowsFormat" => [
      [
        "Aukštų kiekis",
        "Eglės Plotis",
      ],
      [
        function (array $data = []) {
          extract($data);
          return isset($h) ? $h : 'h';
        },
        function (array $data = []) {
          extract($data);
          return isset($h) ? 2 * $h - 1 : '2h - 1';
        },
        "Aukštas",
        "Laisvos vietos iš abiejų pusių",
        "# kiekis"
      ]
    ],
    "dataRowFormat" => [
      null,
      null,
      function (array $data = []) {
        extract($data);
        return isset($n) ? $n : 'n';
      },
      function (array $data = []) {
        extract($data);
        return isset($h) && isset($n) ? $h - $n : 'h - n';
      },
      function (array $data = []) {
        extract($data);
        return isset($n) ? 2 * $n - 1 : '2n - 1';
      }
    ],
    "dataRenderingFunction" => function (?int $h = -1): array {
      if ($h <= 0) return [];
      $data = [
        "headersData" => ["h" => $h],
        "rowsData" => []
      ];
      for ($n = 1; $n <= $h; $n++) {
        $data['rowsData'][] = ["h" => $h, "n" => $n];
      }
      return $data;
    }
  ]
];
